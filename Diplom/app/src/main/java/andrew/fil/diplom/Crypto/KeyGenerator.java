package andrew.fil.diplom.Crypto;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class KeyGenerator {

    public static byte[] generateKey(double k1, double k2, double k3) throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed("any data used as random seed".getBytes());
        double summ = k1 + k2 + k3;
        BigDecimal rDecimal = BigDecimal.valueOf(summ);

        MessageDigest digest = MessageDigest.getInstance("MD5");

        byte[] inputBytes = rDecimal.toBigInteger().toByteArray();

        return digest.digest(inputBytes);
    }
}
