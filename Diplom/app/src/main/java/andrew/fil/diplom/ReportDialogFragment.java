package andrew.fil.diplom;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class ReportDialogFragment extends DialogFragment {
    private String userDataText;
    private String keyText;
    private String encryptedDataText;

    public void setReport(String userData, String key, String encryptedData) {
        userDataText = userData;
        keyText = key;
        encryptedDataText = encryptedData;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_report_dialog, null);
        TextView userData = v.findViewById(R.id.report_userData);
        TextView key = v.findViewById(R.id.report_key);
        TextView encryptedData = v.findViewById(R.id.report_encryptedData);
        userData.setText("Исходные данные:\n        " + userDataText);
        key.setText("Ключ шифрования:\n        " + keyText);
        encryptedData.setText("Зашифрованные данные:\n        " + encryptedDataText);
        builder.setView(v)
                .setTitle(R.string.end_work_title)
                .setPositiveButton(R.string.ok, (dialog, id) -> ((MainActivity) getActivity()).resetWork());
        return builder.create();
    }

}
