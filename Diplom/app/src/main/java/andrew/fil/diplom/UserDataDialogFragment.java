package andrew.fil.diplom;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class UserDataDialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_user_data_dialog, null);
        builder.setView(v)
                .setTitle(R.string.user_data_title)
                .setPositiveButton(R.string.start_work, (dialog, id) -> {
                    EditText fullName = v.findViewById(R.id.fullName);
                    EditText phone = v.findViewById(R.id.phone);
                    MainActivity act = (MainActivity) getActivity();
                    act.setUserData(fullName.getText().toString(), phone.getText().toString());
                });
        return builder.create();
    }
}
