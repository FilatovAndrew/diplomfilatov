package andrew.fil.diplom.API;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface InterfaceAPI {
    @POST("SetKey")
    Call<Integer> SetKey(@Query("key") String key);

    @POST("SetUser")
    Call<Integer> SetUser(@Query("userDataText") String text);
}
