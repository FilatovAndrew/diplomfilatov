package andrew.fil.diplom;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import andrew.fil.diplom.API.API;
import andrew.fil.diplom.API.NetworkManager;
import andrew.fil.diplom.Crypto.CryptoHelper;
import andrew.fil.diplom.Crypto.KeyGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private String userData;
    private ContentValues values;
    private Uri imageUri;
    private int grantResults[];
    private CascadeClassifier face_cascadeClassifier;
    private CascadeClassifier eye_cascadeClassifier;
    private NetworkManager manager;
    private ImageView image;
    private Button btnStartWork;
    private Button btnShowReport;
    private String keyText;
    private String encryptedUserData;

    // Константы
    private final int ExternalStoragePermissionRequestCode = 12;
    private final int ActivityResultRequestCode = 14;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Инициализация главного изображение
        image = findViewById(R.id.image);
        image.setVisibility(View.GONE);

        // Инициализация кнопки отображение отчета
        btnShowReport = findViewById(R.id.showReport);
        btnShowReport.setVisibility(View.GONE);
        btnShowReport.setOnClickListener(v -> {
            ReportDialogFragment dialog = new ReportDialogFragment();
            dialog.setReport(userData, keyText, encryptedUserData);
            dialog.show(getSupportFragmentManager(), "Report");
        });

        // Инициализация кнопки начала работы
        btnStartWork = findViewById(R.id.start_work);
        btnStartWork.setVisibility(View.GONE);
        btnStartWork.setOnClickListener(v -> {
            values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, ActivityResultRequestCode);
        });

        // Инициализация OpenCV
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_8, this, mLoaderCallback);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    InitCascadeClassifiers();
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 30);
                        onRequestPermissionsResult(ExternalStoragePermissionRequestCode, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, grantResults);
                    } else {
                        showUserDataDialog();
                    }
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode != ExternalStoragePermissionRequestCode)
            return;
        if (grantResults == null)
            return;

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showUserDataDialog();
        } else {
            Toast.makeText(MainActivity.this, "Нет разрешения!", Toast.LENGTH_SHORT).show();
            onDestroy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ActivityResultRequestCode != requestCode || resultCode != RESULT_OK)
            return;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            double[] distances = analyzeFace(bitmap);
            byte[] key = KeyGenerator.generateKey(distances[0], distances[1], distances[2]);
            SetKeyAndUser(key, CryptoHelper.encrypt(userData, key));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUserDataDialog() {
        UserDataDialogFragment dialog = new UserDataDialogFragment();
        dialog.show(getSupportFragmentManager(), "UserData");
    }

    private void SetKeyAndUser(byte[] key, byte[] userData) {
        manager = new NetworkManager(new API());

        keyText = Base64.encodeToString(key, Base64.DEFAULT);
        encryptedUserData = Base64.encodeToString(userData, Base64.DEFAULT);

        manager.SetKey(keyText, new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.body() == null)
                    return;

                int resultCode = response.body();
                if (resultCode != 1)
                    return;

                manager.SetUser(encryptedUserData, new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        if (response.body() == null)
                            return;

                        btnShowReport.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {
                        Log.e("NetworkManager", "SetUser Failure");
                    }
                });
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e("NetworkManager", "SetKey Failure");
            }
        });
    }

    private void InitCascadeClassifiers() {
        try {
            InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_alt);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            InputStream e_is = getResources().openRawResource(R.raw.haarcascade_eye);
            File e_cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File e_mCascadeFile = new File(e_cascadeDir, "haarcascade_eye.xml");
            FileOutputStream e_os = new FileOutputStream(e_mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            byte[] e_buffer = new byte[4096];
            int e_bytesRead;
            while ((e_bytesRead = e_is.read(e_buffer)) != -1) {
                e_os.write(e_buffer, 0, e_bytesRead);
            }
            e_is.close();
            e_os.close();

            face_cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            face_cascadeClassifier.load(mCascadeFile.getAbsolutePath());

            eye_cascadeClassifier = new CascadeClassifier(e_mCascadeFile.getAbsolutePath());
            eye_cascadeClassifier.load(e_mCascadeFile.getAbsolutePath());

        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

    }

    private double[] analyzeFace(Bitmap bitmap) {
        Mat mat = new Mat();
        Scalar redColor = new Scalar(0, 255, 0);
        Scalar otherColor = new Scalar(155, 155, 155);
        Utils.bitmapToMat(bitmap, mat);

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY, 4);

        MatOfRect faceDetections = new MatOfRect();
        MatOfRect e_faceDetections = new MatOfRect();
        face_cascadeClassifier.detectMultiScale(mat, faceDetections);
        eye_cascadeClassifier.detectMultiScale(mat, e_faceDetections);

        Point centerFace = new Point();
        if (faceDetections.toArray().length > 0) {
            Rect rect_face = faceDetections.toArray()[0];
            Imgproc.rectangle(mat, new Point(rect_face.x, rect_face.y),
                    new Point(rect_face.x + rect_face.width, rect_face.y + rect_face.height),
                    redColor);
            centerFace = new Point(rect_face.x + rect_face.width / 2, rect_face.y + rect_face.height / 2);
            Imgproc.circle(mat, centerFace, 3, otherColor);
        }

        Point centerRightEye = new Point();
        Point centerLeftEye = new Point();
        double distFromLeftEyeToCenterFace;
        double distFromRightEyeToCenterFace;
        double distBetweenEyes;

        // Обводка черт лица
        if (e_faceDetections.toArray().length > 1) {
            Rect rect_eye_1 = e_faceDetections.toArray()[0];
            Rect rect_eye_2 = e_faceDetections.toArray()[1];
            Imgproc.rectangle(mat, new Point(rect_eye_1.x, rect_eye_1.y),
                    new Point(rect_eye_1.x + rect_eye_1.width, rect_eye_1.y + rect_eye_1.height),
                    redColor);
            Imgproc.rectangle(mat, new Point(rect_eye_2.x, rect_eye_2.y),
                    new Point(rect_eye_2.x + rect_eye_2.width, rect_eye_2.y + rect_eye_2.height),
                    redColor);

            centerRightEye = new Point(rect_eye_1.x + rect_eye_1.width / 2, rect_eye_1.y + rect_eye_1.height / 2);
            centerLeftEye = new Point(rect_eye_2.x + rect_eye_2.width / 2, rect_eye_2.y + rect_eye_2.height / 2);
            Imgproc.circle(mat, centerLeftEye, 3, otherColor);
            Imgproc.circle(mat, centerRightEye, 3, otherColor);
        }

        // Расчет расстояний
        distFromLeftEyeToCenterFace = Math.sqrt(
                Math.pow(centerFace.x - centerLeftEye.x, 2) +
                        Math.pow(centerFace.y - centerLeftEye.y, 2));
        distFromRightEyeToCenterFace = Math.sqrt(
                Math.pow(centerFace.x - centerRightEye.x, 2) +
                        Math.pow(centerFace.y - centerRightEye.y, 2));
        distBetweenEyes = Math.sqrt(
                Math.pow(centerLeftEye.x - centerRightEye.x, 2) +
                        Math.pow(centerLeftEye.y - centerRightEye.y, 2));
        Utils.matToBitmap(mat, bitmap);
        btnStartWork.setVisibility(View.GONE);
        image.setImageBitmap(bitmap);
        image.setVisibility(View.VISIBLE);


        // Массив расстояний
        double[] returnedArray = {distFromLeftEyeToCenterFace, distFromRightEyeToCenterFace, distBetweenEyes};
        return returnedArray;
    }

    //region Public Interface
    public void setUserData(String FIO, String phone) {
        userData = FIO + "_" + phone;
        btnStartWork.setVisibility(View.VISIBLE);
    }

    public void resetWork() {
        showUserDataDialog();
        btnStartWork.setVisibility(View.VISIBLE);
        btnShowReport.setVisibility(View.GONE);
        image.setVisibility(View.GONE);
    }
    //endregion
}
