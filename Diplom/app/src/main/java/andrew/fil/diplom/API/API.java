package andrew.fil.diplom.API;

import andrew.fil.diplom.Models.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class API implements InterfaceAPI {
    private InterfaceAPI m_DiplomApi;

    public API() {
        Retrofit ret = new Retrofit.Builder()
                .baseUrl("http://62.109.17.98/home/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
        m_DiplomApi = ret.create(InterfaceAPI.class);
    }

    @Override
    public Call<Integer> SetKey(String token) {
        return m_DiplomApi.SetKey(token);
    }

    @Override
    public Call<Integer> SetUser(String token) {
        return m_DiplomApi.SetUser(token);
    }
}
