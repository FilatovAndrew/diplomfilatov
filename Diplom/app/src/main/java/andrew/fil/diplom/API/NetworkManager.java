package andrew.fil.diplom.API;

import retrofit2.Callback;

public class NetworkManager {
    public NetworkManager(API api) {
        m_Api = api;
    }

    private API m_Api;

    public void SetUser(String encryptedUserData, Callback<Integer> callback) {
        m_Api.SetUser(encryptedUserData).enqueue(callback);
    }

    public void SetKey(String token, Callback<Integer> callback) {
        m_Api.SetKey(token).enqueue(callback);
    }
}