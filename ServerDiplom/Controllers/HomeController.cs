﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServerDiplom.Models;

namespace ServerDiplom.Controllers
{
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private static byte[] m_SessionKey;

        private readonly DiplomContext m_Context;

        public HomeController(DiplomContext context)
        {
            m_Context = context;
        }

        [Route("SetKey")]
        [HttpPost]
        public int SetKey([FromQuery] string key)
        {
            try
            {
                Console.WriteLine(key);
                if (key.Length > 0)
                    m_SessionKey = Convert.FromBase64String(key);
                else return -1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }

            return 1;
        }

        [Route("SetUser")]
        [HttpPost]
        public int SetUser([FromQuery]string userDataText)
        {
            try
            {
                var userData = Convert.FromBase64String(userDataText);
                var plainText = CryptoHelper.Decrypt(userDataText, m_SessionKey);
                var splittedText = plainText.Split('_');
                var fullname = splittedText[0];
                var phone = Convert.ToInt32(splittedText[1]);
                var newUser = new User()
                {
                    FullName = fullname,
                    PhoneNumber = phone
                };
                m_Context.Users.Add(newUser);
                return 1;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                return -1;
            }
        }
    }
}
