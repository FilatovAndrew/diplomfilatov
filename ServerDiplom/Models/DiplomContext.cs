using Microsoft.EntityFrameworkCore;

namespace ServerDiplom.Models
{
    public class DiplomContext : DbContext
    {
        public DiplomContext(DbContextOptions<DiplomContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
    }
}