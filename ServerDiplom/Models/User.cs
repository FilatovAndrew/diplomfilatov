namespace ServerDiplom.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int PhoneNumber { get; set; }
    }
}